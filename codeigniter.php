<?php

/*
 * ------------------------------------------------------
*  check authority
* ------------------------------------------------------
*/
require_once(APPPATH.'core/PW_Authentication.php');
switch (current_module()){
    case 'admin':
        //chk_admin();
        break;
    case 'staff':
        //chk_staff();
        break;
    case 'cgi':
        //chk_cgi();
        break;
    default:
        break;
}

/**
 * Fake class for Eclipse convenience purpose.
 */
class codeigniter
{
    // NEW MODEL
    /**
     * @var App_common */           private $app_common;
    /**
     * @var Account */              private $account;
    /**
     * @var Action */               private $action;
    /**
     * @var Agreement */            private $agreement;
    /**
     * @var Arrival_plan */         private $arrival_plan;
    /**
     * @var Booth */                private $booth;
    /**
     * @var Campaign */             private $campaign;
    /**
     * @var Clearance_fee */        private $clearance_fee;
    /**
     * @var Consumption_tax */      private $consumption_tax;
    /**
     * @var Contract_settlement */  private $contract_settlement;
    /**
     * @var Contract_status */      private $contract_status;
    /**
     * @var Contract */             private $contract;
    /**
     * @var Customer */             private $customer;
    /**
     * @var Delivery_cycle */       private $delivery_cycle;
    /**
     * @var Delivery */             private $delivery;
    /**
     * @var Delivery_company */     private $delivery_company;
    /**
     * @var Direct_debit_setting */ private $direct_debit_setting;
    /**
     * @var Fee */                  private $fee;
    /**
     * @var Generic */              private $generic;
    /**
     * @var Holiday */              private $holiday;
    /**
     * @var Information */          private $information;
    /**
     * @var Inventory */            private $inventory;
    /**
     * @var Import_product_log */   private $import_product_log;
    /**
     * @var Partner_settlement */   private $partner_settlement;
    /**
     * @var Partner */              private $partner;
    /**
     * @var Paygent_model */        private $paygent_model;
    /**
     * @var Prefecture */           private $prefecture;
    /**
     * @var Product */              private $product;
    /**
     * @var Product_attribute */    private $product_attribute;
    /**
     * @var Pulldown */             private $pulldown;
    /**
     * @var Paygent_setting */      private $paygent_setting;
    /**
     * @var Report_model */         private $report_model;
    /**
     * @var Resource */             private $resource;
    /**
     * @var Server */               private $server;
    /**
     * @var Settlement_type */      private $settlement_type;
    /**
     * @var Shipment */             private $shipment;
    /**
     * @var Staff */                private $staff;
    /**
     * @var Stock */                private $stock;
    /**
     * @var Stock_min_quantity */   private $stock_min_quantity;
    /**
     * @var System_code */          private $system_code;
    /**
     * @var Sbps */                 private $sbps;
    /**
     * @var Warehouse */            private $warehouse;
    /**
     * @var Water_company */        private $water_company;
    /**
     * @var Zipcode */              private $zipcode;
    /**
     * @var Order */                private $order;
    /**
     * @var Np_setting */           private $np_setting;
    /**
     * @var Order_settlement */     private $order_settlement;
    /**
     * @var Supplement */           private $supplement;
    /**
     * @var Hydrogen_processing */  private $hydrogen_processing;
    /**
     * @var Contract_service_hydrogen */ private $contract_service_hydrogen;
    /**
     * @var Hydrogen_discount */    private $hydrogen_discount;
    /**
     * @var Hydrogen_fee */         private $hydrogen_fee;
    /**
     * @var Hydrogen_product */     private $hydrogen_product;

    // NEW LIBRARY
    /**
     * @var Auth */                 private $auth;
    /**
     * @var Email_template */       private $email_template;
    /**
     * @var Feed */                 private $feed;
    /**
     * @var Header */               private $header;
    /**
     * @var Template */             private $template;
    /**
     * @var Net_protection */       private $net_protection;
    /**
     * @var PW_Sbps */              private $pw_sbps;
    // SYSTEM CORE AND LIBRARY
    /**
     * @var CI_User_agent */        private $agent;
    /**
     * @var CI_Benchmark */         private $benchmark;
    /**
     * @var CI_Calendar */          private $calendar;
    /**
     * @var CI_Cart */              private $cart;
    /**
     * @var CI_Config */            private $config;
    /**
     * @var PW_DB_active_rec */     private $db;
    /**
     * @var CI_Email */             private $email;
    /**
     * @var CI_Encrypt */           private $encrypt;
    /**
     * @var PW_Form_validation */   private $form_validation;
    /**
     * @var CI_FTP */               private $ftp;
    /**
     * @var CI_Image_lib */         private $image_lib;
    /**
     * @var PW_Input */             private $input;
    /**
     * @var PW_Lang */              private $lang;
    /**
     * @var CI_Output */            private $output;
    /**
     * @var PW_Pagination */        private $pagination;
    /**
     * @var PW_Parser */            private $parser;
    /**
     * @var PW_Session */           private $session;
    /**
     * @var PW_Table */             private $table;
    /**
     * @var CI_Trackback */         private $trackback;
    /**
     * @var CI_Typography */        private $typography;
    /**
     * @var CI_Unit_test */         private $unit;
    /**
     * @var PW_Upload */            private $upload;
    /**
     * @var CI_URI */               private $uri;
    /**
     * @var CI_Xmlrpc */            private $xmlrpc;
    /**
     * @var CI_Xmlrpcs */           private $xmlrpcs;
    /**
     * @var PW_Loader */            private $load;
    /**
     * @var PW_Router */            private $router;
    /**
     * @var CI_Zip */               private $zip;
    /**
     * @var PW_Profiler */          private $profiler;
    /**
     * @var Order_acquisition */    private $order_acquisition;
    /**
     * @var Push_information */     private $push_information;
    /**
     * @var Report_shipment */      private $report_shipment;
}

?>
