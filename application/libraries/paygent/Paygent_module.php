<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/** *********************************************************************
 * ==================================================================
 * ペイジェントモジュールクラス
 * ==================================================================
 * PHP Versions 4/5.
 *
 * @package        CodeIgniter
 * @subpackage     application/libraries
 * @author         Isao Namamura
 * @since          2010-10-05.
 * @history        -
 ** *********************************************************************/
//Load define paygent file
require_once(APPPATH . 'libraries/paygent/Paygent_define.php');

//Add path to paygent module
set_include_path(get_include_path() . PATH_SEPARATOR . PAYGENT_PATH);

//Load module paygent
include_once("jp/co/ks/merchanttool/connectmodule/entity/ResponseDataFactory.php");
include_once("jp/co/ks/merchanttool/connectmodule/system/PaygentB2BModule.php");

//Load exception paygent
include_once("jp/co/ks/merchanttool/connectmodule/exception/PaygentB2BModuleConnectException.php");
include_once("jp/co/ks/merchanttool/connectmodule/exception/PaygentB2BModuleException.php");

class Paygent_module {

    var $CI;

    var $enc_code = 'UTF-8';

    /**
     * 電文種別
     */
    var $telegramtype = '';

    /**
     * 文字コードの変換を行う
     *
     * @param        void
     * @return        $rtnPrefData
     * @access        public
     */
    function __convert_moji($str){
        return mb_convert_encoding($str,$this->enc_code,'Shift_JIS');
    }

    /**
     * ペイジェント接続モジュールクラス
     */
    var $pgntmodule = '';

    /**
     * コンストラクタ。
     *
     * @param        void
     * @return        void
     * @access        public
     */
    function __construct () {
        $this->CI =& get_instance();
        $this->pgntmodule = new PaygentB2BModule();
        $this->pgntmodule->init();
    }
    /**
     * バックアップファイル作成
     *
     * @param        void
     * @return        void
     * @access        public
     */
    function makeFile($path, $fileName, $strFileData) {
        $fp = fopen($path . $fileName, "w");
        fwrite($fp, $strFileData);
        fclose($fp);
        chmod($path . $fileName, 0777);
    }

    /**
     * ファイル決済照会電文処理
     *
     * @param        string        $pathToView
     * @param        array        $array
     * @return        void
     */
    function filetyperesult($pParam) {

        //# ログファイルの出力
        $this->CI->log->write_flog('paygent-debug',"[filetyperesult]");

        //# 電文パラメータの設定
        $pPrm['trading_id']        =    $pParam['trading_id'];        // マーチャント取引ＩＤ
        $pPrm['payment_id']        =    '';                           // 決済ＩＤ
        $pPrm['telegram_kind']     =    KIND_FILE_RESULT;             // 電文種別ＩＤ(ファイル決済照会)
        $pPrm['file_receipt_id']   =    $pParam['file_receipt_id'];   // 受付番号
        $pPrm['file_receipt_name'] =    $pParam['file_receipt_name']; // 結果ファイル名
        $pPrm['merchant_id']       =    $pParam["merchant_id"];       // マーチャントID
        $pPrm['connect_id']        =    $pParam["connect_id"];        // 接続ID
        $pPrm['connect_password']  =    $pParam["connect_password"];  // 接続パスワード
        $pPrm['client_filename']   =    $pParam["client_filename"];   // 認証ファイル名

        //# 電文パラメータの設定：共通ヘッダー部の生成
        $this->__setHeaderTelegram($pPrm);
        //# 電文パラメータの設定：個別詳細情報
        $this->__setDetailTelegram($pPrm);
        //# 電文送信処理
        $rtnData = $this->__sendTelegram();
        //# 応答データのセット
        $pPrm['post_result'] = $rtnData;

        //# ログファイルの出力
//         $this->CI->log->write_flog('paygent-debug',"[/filetyperesult]");

        //# 応答データ処理＆戻り値の設定
        return $pPrm['post_result'];
    }

    /**
     * ファイル決済要求電文処理
     *
     * @param        string        $pathToView
     * @param        array        $array
     * @return        void
     */
    function filetypepayment($pParam) {

        //# ログファイルの出力
        $this->CI->log->write_flog('paygent-debug', "[filetypepayment]");

        //# 電文パラメータの設定
        $pPrm['trading_id']       =    $pParam['trading_id'];        // マーチャント取引ＩＤ
        $pPrm['payment_id']       =    '';                           // 決済ＩＤ
        $pPrm['telegram_kind']    =    KIND_FILE_PAYMENT;            // 電文種別ＩＤ(ファイル決済要求)
        $pPrm['file_type']        =    $pParam['file_type'];         // 取引種別
        $pPrm['data']             =    $pParam['data'];              // 取引ファイルデータ
        $pPrm['merchant_id']      =    $pParam["merchant_id"];       // マーチャントID
        $pPrm['connect_id']       =    $pParam["connect_id"];        // 接続ID
        $pPrm['connect_password'] =    $pParam["connect_password"];  // 接続パスワード
        $pPrm['client_filename']  =    $pParam["client_filename"];   // 認証ファイル名

        //# 電文パラメータの設定：共通ヘッダー部の生成
        $this->__setHeaderTelegram($pPrm);
        //# 電文パラメータの設定：個別詳細情報
        $this->__setDetailTelegram($pPrm);
        //# 電文送信処理
        $rtnData = $this->__sendTelegram();
        //# 応答データのセット
        $pPrm['post_result'] = $rtnData;

        //# ログファイルの出力
        //$this->CI->log->write_flog('paygent-debug',print_r($pPrm, true));
        $this->CI->log->write_flog('paygent-debug',"[/filetypepayment]");

        //# 応答データ処理＆戻り値の設定
        return $pPrm['post_result'];
    }

    /**
     * カード情報お預かり電文処理
     *
     * @param        string       $pathToView
     * @param        array        $array
     * @return        void
     */
    function setCreditCardInfo($pParam) {

        //# 電文パラメータの設定
        $pPrm['trading_id']       =    '';                           // マーチャント取引ＩＤ
        $pPrm['payment_id']       =    '';                           // 決済ＩＤ
        $pPrm['telegram_kind']    =    KIND_CARD_INFO_SET;           // 電文種別ＩＤ(カード情報設定)
        $pPrm['customer_id']      =    $pParam['customer_id'];       // 顧客ＩＤ
        $pPrm['card_number']      =    $pParam['card_number'];       // カード番号
        $pPrm['card_valid_term']  =    $pParam['card_valid_term'];   // カード有効期限
        $pPrm['card_brand']       =    '';                           // カードブランド
        $pPrm['merchant_id']      =    $pParam["merchant_id"];       // マーチャントID
        $pPrm['connect_id']       =    $pParam["connect_id"];        // 接続ID
        $pPrm['connect_password'] =    $pParam["connect_password"];  // 接続パスワード
        $pPrm['client_filename']  =    $pParam["client_filename"];   // 認証ファイル名

        // 電文パラメータの設定：共通ヘッダー部の生成
        $this->__setHeaderTelegram($pPrm);
        // 電文パラメータの設定：個別詳細情報
        $this->__setDetailTelegram($pPrm);
        // 電文送信処理
        $rtnData = $this->__sendTelegram();
        // 応答データのセット
        $pPrm['post_result'] = $rtnData;

        //# 応答データ処理＆戻り値の設定
        return $pPrm['post_result'];
    }

    /**
     * カード決済売上キャンセル電文処理（2012.8.31：宮寺）
     *
     * @param        string        $pathToView
     * @param        array        $array
     * @return        void
     */
    function captureCancelCardpayment($pParam) {

        //# ログファイルの出力
        $this->CI->log->write_flog('paygent-debug',"[captureCancelCardpayment]");

        //# 電文パラメータの設定
        $pPrm['trading_id']       = $pParam['trading_id'];        // マーチャント取引ＩＤ
        $pPrm['payment_id']       = $pParam['payment_id'];        // 決済ＩＤ
        $pPrm['telegram_kind']    = KIND_CARD_CAPTURE_CANCEL;     // 電文種別ＩＤ(カード決済売上キャンセル)
        $pPrm['merchant_id']      = $pParam["merchant_id"];       // マーチャントID
        $pPrm['connect_id']       = $pParam["connect_id"];        // 接続ID
        $pPrm['connect_password'] = $pParam["connect_password"];  // 接続パスワード
        $pPrm['client_filename']  = $pParam["client_filename"];   // 認証ファイル名

        // 電文パラメータの設定：共通ヘッダー部の生成
        $this->__setHeaderTelegram($pPrm);
        // 電文送信処理
        $rtnData = $this->__sendTelegram();
        // 応答データのセット
        $pPrm['post_result'] = $rtnData;

        //# ログファイルの出力
        $this->CI->log->write_flog('paygent-debug',print_r($pPrm, true));
        $this->CI->log->write_flog('paygent-debug',"[/captureCancelCardpayment]");

        //# 応答データ処理＆戻り値の設定
        return $pPrm['post_result'];
    }

    /**
     * カード決済補正売上電文処理（2012.8.31：宮寺）
     *
     * @param        string        $pathToView
     * @param        array        $array
     * @return        void
     */
    function captureChangeCardpayment($pParam) {

        //# ログファイルの出力
        $this->CI->log->write_flog('paygent-debug',"[captureChangeCardpayment]");

        // 電文パラメータの設定
        $pPrm['trading_id']            = $pParam['trading_id'];        // マーチャント取引ＩＤ
        $pPrm['payment_id']            = $pParam['payment_id'];        // 決済ＩＤ
        $pPrm['telegram_kind']         = KIND_CARD_CAPTURE_MODIFY;     // 電文種別ＩＤ(カード決済補正売上)
        $pPrm['payment_amount']        = $pParam['payment_amount'];    // 決済金額
        $pPrm['reduction_flag']        = $pParam['reduction_flag'];    // 減額フラグ
        $pPrm['merchant_id']           = $pParam["merchant_id"];       // マーチャントID
        $pPrm['connect_id']            = $pParam["connect_id"];        // 接続ID
        $pPrm['connect_password']      = $pParam["connect_password"];  // 接続パスワード
        $pPrm['client_filename']       = $pParam["client_filename"];   // 認証ファイル名

        // 電文パラメータの設定：共通ヘッダー部の生成
        $this->__setHeaderTelegram($pPrm);
        // 電文パラメータの設定：個別詳細情報
        $this->__setDetailTelegram($pPrm);
        // 電文送信処理
        $rtnData = $this->__sendTelegram();

        // 応答データのセット
        $pPrm['post_result'] = $rtnData;

        //# ログファイルの出力
        $this->CI->log->write_flog('paygent-debug',print_r($pPrm, true));
        $this->CI->log->write_flog('paygent-debug',"[/captureChangeCardpayment]");

        // 応答データ処理＆戻り値の設定
        return $pPrm['post_result'];

    }

    /**
     * 電文送信処理
     *
     * @param        array        $pParam
     * @return        void
     */
    function __sendTelegram() {

        $aryRtn      = array();
        $aryResponse = array();
        $curResponse = '';

        // エラー場所の設定
        $aryRtn['errtype'] = '__sendTelegram()';

        //# クライアント証明書パスを設定する
        $this->pgntmodule->clientFilePath = $this->pgntmodule->masterFile->getClientFilePath();

        //# ペイジェントへ要求の送信
        $rtn = array(
            'result' => true,
            'response' => array(
                'customer_card_id' => $this->pgntmodule->telegramParam['card_number']
            )
        );
return $rtn;

        //# ログファイルの出力
        $this->CI->log->write_flog('paygent-debug','    -> __sendTelegram()');

        //# 電文処理が正常に終了したかどうか？
        if ($rtn === TELEGRAM_POST_ERR) {
            //# ログファイルの出力
            $this->CI->log->write_flog('paygent-debug','    -> [ERR]999:電文処理エラー');

            //# 戻り値の設定
            $aryRtn['result']   = FALSE;
            $aryRtn['telegram'] = $this->telegramtype;
            $aryRtn['errtype']  = '__sendTelegram()';
            $aryRtn['errcode']  = '999';
            $aryRtn['errmsg']   = '電文処理エラー';
            return $aryRtn;
        }

        //# レスポンス情報を取得する
        $aryReponseInfo = array(
                                'code'     => $this->pgntmodule->getResponseCode(),
                                'detail'   => $this->__convert_moji($this->pgntmodule->getResponseDetail())
                            );

        //# 処理結果が「異常」の場合は、エラー内容を取得する
        if ($this->pgntmodule->getResultStatus() == TELEGRAM_RESULT_ERR) {

            //# ログファイルの出力
            $this->CI->log->write_flog('paygent-debug','    -> [ERR]' . $aryReponseInfo['code'] . ':' . $aryReponseInfo['detail']);

            //# 戻り値の設定
            $aryRtn['result']   = FALSE;
            $aryRtn['telegram'] = $this->telegramtype;
            $aryRtn['errtype']  = '__sendTelegram()';
            $aryRtn['errcode']  = $aryReponseInfo['code'];
            $aryRtn['errmsg']   = $aryReponseInfo['detail'];
            return $aryRtn;

        } else {
            //# ファイル決済結果照会は応答電文レコードがないのでレコード取得は行わない
            if ($this->telegramtype == KIND_FILE_RESULT) {
                $aryResponse = '';
            } else {
                //# 応答電文が存在するか？
                if($this->pgntmodule->hasResNext()){
                    //# 応答電文レコードの取得
                    $curResponse = $this->pgntmodule->resNext();
                    $aryResponse = $curResponse;
                } else {
                    //# ログファイルの出力
                    $this->CI->log->write_flog('paygent-debug','    -> [ERR]' . $aryReponseInfo['code'] . ':' . $aryReponseInfo['detail']);

                    //# 戻り値の設定
                    $aryRtn['result']   = FALSE;
                    $aryRtn['telegram'] = $this->telegramtype;
                    $aryRtn['errtype']  = '__sendTelegram()';
                    $aryRtn['errcode']  = $aryReponseInfo['code'];
                    $aryRtn['errmsg']   = $aryReponseInfo['detail'];
                    return $aryRtn;
                }
            }
        }

        //# 戻り値の設定
        $aryRtn['result']    = TRUE;
        $aryRtn['telegram']  = $this->telegramtype;
        $aryRtn['errtype']   = '__sendTelegram()';
        $aryRtn['errcode']   = 0;
        $aryRtn['errmsg']    = '';
        $aryRtn['response']  = $aryResponse;

        $this->CI->log->write_flog('paygent-debug','    -> [OK]');

        return $aryRtn;

    }

    /**
     * 電文パラメータの設定：個別詳細情報＠ファイル決済照会
     *
     * @param        array        $pParam
     * @return        void
     */
    function __fileresultTelegram($pParam){

        //# ログファイルの出力
        $this->CI->log->write_flog('paygent-debug',"    -> __fileresultTelegram()");

        //# 各種パラメータの設定
        $this->pgntmodule->reqPut("file_receipt_id", $pParam['file_receipt_id']);               // 受付番号
        $this->pgntmodule->setResultCsv(PGNT_FILE_RECV_PATH . $pParam['file_receipt_name']);    // 結果CSVファイルのパス

        //# ログファイルの出力
        $strlog = '        -> file_receipt_id:' . $pParam['file_receipt_id'];
        $this->CI->log->write_flog('paygent-debug',$strlog);

    }

    /**
     * 電文パラメータの設定：個別詳細情報＠ファイル決済要求
     *
     * @param        array        $pParam
     * @return        void
     */
    function __filepaymentTelegram($pParam){

        //# ログファイルの出力
        $this->CI->log->write_flog('paygent-debug',"    -> __filepaymentTelegram()");

        //# 各種パラメータの設定
        $this->pgntmodule->reqPut("file_type", $pParam['file_type']);    //# 取引種別
        $this->pgntmodule->reqPut("data",      $pParam['data']);         //# 取引ファイルデータ

        //# ログファイルの出力
        $strlog = '        -> file_type:' . $pParam['file_type']
                . '／data:(length)' . strlen($pParam['data']);
        $this->CI->log->write_flog('paygent-debug', $strlog);

    }

    /**
     * 電文パラメータの設定：個別詳細情報＠カード情報お預かり機能
     *
     * @param        array        $pParam
     * @return        void
     */
    function __cardKeepingTelegram($pParam){

        //# ログファイルの出力
        $this->CI->log->write_flog('paygent-debug',"    -> __cardKeepingTelegram()");

        //# 各種パラメータの設定
        $this->pgntmodule->reqPut("customer_id",     $pParam['customer_id']);       //# 顧客ＩＤ
        $this->pgntmodule->reqPut("card_number",     $pParam['card_number']);       //# カード番号
        $this->pgntmodule->reqPut("card_valid_term", $pParam['card_valid_term']);   //# カード有効期限
        $this->pgntmodule->reqPut("card_brand",      $pParam['card_brand']);        //# カードブランド
        $this->pgntmodule->reqPut("cardholder_name", '');                           //# カード名義人
        $this->pgntmodule->reqPut("add_info1",       '');                           //# 補足情報1
        $this->pgntmodule->reqPut("add_info2",       '');                           //# 補足情報2
        $this->pgntmodule->reqPut("add_info3",       '');                           //# 補足情報3
        $this->pgntmodule->reqPut("add_info4",       '');                           //# 補足情報4
        $this->pgntmodule->reqPut("site_id",         '');                           //# サイトＩＤ
        $this->pgntmodule->reqPut("valid_check_flg",'1');                           //# 有効性チェックフラグ(1:確認する)

        //# ログファイルの出力
        $strlog = '        -> customer_id:' . $pParam['customer_id']
                . '／card_number:' . substr($pParam['card_number'],0,5) . '********'
                . '／card_valid_term:' . $pParam['card_valid_term']
                . '／merchant_id:' . $pParam['merchant_id']
                . '／connect_id:' . $pParam['connect_id']
                . '／connect_password:' . $pParam['connect_password']
                . '／client_filename:' . $pParam['client_filename'];
        $this->CI->log->write_flog('paygent-debug',$strlog);
    }
    /**
     * 電文パラメータの設定：個別詳細情報＠カード決済補正売上（2012.8.31：宮寺）
     *
     * @param        array        $pParam
     * @return        void
     */
    function __cardCaptureChangeTelegram($pParam){

        //# ログファイルの出力
        $this->CI->log->write_flog('paygent-debug',"    -> __cardCaptureChangeTelegram()");

        //# 各種パラメータの設定
        $this->pgntmodule->reqPut("payment_amount", $pParam['payment_amount']); //# 決済金額
        $this->pgntmodule->reqPut("reduction_flag", $pParam['reduction_flag']); //# 減額フラグ

        //# ログファイルの出力
        $strlog = '        -> payment_amount:' . $pParam['payment_amount']
                . '／reduction_flag:' . $pParam['reduction_flag'];
        $this->CI->log->write_flog('paygent-debug',$strlog);
    }
    /**
     * 電文パラメータの設定：共通ヘッダー部
     *
     * @param        array        $pParam
     * @return        void
     */
    function __setHeaderTelegram($pParam){

        $this->CI->load->library('encrypt');
//[START][11318][13727][CHG] - [dinh_sinh] - [2016/09/22] - reset key decode
//         $this->CI->encrypt->set_cipher(MCRYPT_BLOWFISH);
//         $pParam['connect_password'] = $this->CI->encrypt->decode($pParam['connect_password']);
        $cipher_before = $this->CI->encrypt->_get_cipher();
        $this->CI->encrypt->set_cipher(MCRYPT_BLOWFISH);
        $pParam['connect_password'] = $this->CI->encrypt->decode($pParam['connect_password']);
        //reset cipher key before.
        $this->CI->encrypt->set_cipher($cipher_before);
//[END][11318][13727][CHG] - [dinh_sinh] - [2016/09/22]

        $this->pgntmodule->reqPut("merchant_id",      $pParam['merchant_id']);                          //# マーチャントＩＤ
        $this->pgntmodule->reqPut("connect_id",       $pParam['connect_id']);                           //# 接続ＩＤ
        $this->pgntmodule->reqPut("connect_password", $pParam['connect_password']);                     //# 接続パスワード
        $this->pgntmodule->masterFile->setClientFilePath(PAYGENT_CERTIFICATE_PATH . $pParam['client_filename']);    //# クライアント証明書パスの設定

        //# 各種パラメータの設定
        $this->pgntmodule->reqPut("telegram_kind",    $pParam['telegram_kind']);        //# 電文種別ＩＤ
        $this->pgntmodule->reqPut("telegram_version", PAYGENT_TELEGRAM_VERSION);        //# 電文バージョン番号
        $this->pgntmodule->reqPut("trading_id",       $pParam["trading_id"]);           //# マーチャント取引ＩＤ
        $this->pgntmodule->reqPut("payment_id",       $pParam["payment_id"]);           //# 決済ＩＤ

        //# 電文種別を格納する
        $this->telegramtype = $pParam['telegram_kind'];
    }

    /**
     * 電文パラメータの設定：個別詳細情報
     *
     * @param        array        $pParam
     * @return        void
     */
    function __setDetailTelegram($pParam){

        switch($pParam['telegram_kind']){
            //# -----------------------------
            //# カード決済補正売上（2012.8.31：宮寺）
            //# -----------------------------
            case KIND_CARD_CAPTURE_MODIFY:
                $this->__cardCaptureChangeTelegram($pParam);
                break;
            //# カード決済売上キャンセル
            //# -----------------------------
            case KIND_CARD_CAPTURE_CANCEL:
                break;
            //# -----------------------------
            //# カード情報設定
            //# -----------------------------
            case KIND_CARD_INFO_SET:
                $this->__cardKeepingTelegram($pParam);
                break;
            //# -----------------------------
            //# ファイル決済要求
            //# -----------------------------
            case KIND_FILE_PAYMENT:
                $this->__filepaymentTelegram($pParam);
                break;
            //# -----------------------------
            //# ファイル決済結果照会
            //# -----------------------------
            case KIND_FILE_RESULT:
                $this->__fileresultTelegram($pParam);
                break;
        }
    }
}
?>