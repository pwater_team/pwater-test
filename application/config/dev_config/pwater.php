<?php
/**
 * config information for net protection payment method
 * nt: net protection
 * @author van_don
 */
$config['np_url'] = 'https://www.getsafety-online.com/np_ct/npcb/services/NPConnectPro?wsdl'; //Test link

// IVR get_discernment_id_url
$config['ivr_get_discernment_id_url']    = 'https://dhkdemo.spv.jp/cardlink-tk/discernmentIdGet/PremiumWater'; //Test link

//Product link
//test link
$config['membership_life_tea_url'] = 'https://reg34.smp.ne.jp/regist/is?SMPFORM=mjke-rfrhp-8bea8f6e3010394557aa8a2ca497ffaa';

/*
 * Nifty Cloud Application key
 */
$config['nifty_application_key'] = '83c0a18b09253d402ac88635ebe700410b73b56a4ae8955f4c65a813ed09a1de';

/*
 * Nifty Cloud Client key
 */
$config['nifty_client_key'] = '6e84066534b15f55146c44a0974c1a5cfbb4b5488c6a9c91832ff3d94b9c6e55';

//[Start][11318][11606] - [cu_lac] - [14/06/2016] - config sbps info
$config['sbps_payment'] = array(
        'api_info'  => array(
                'merchant_id'       => '71743',
                'service_id'        => '001',
                'pagecon_url'       => '/_cgi/sbps_process/register',
                'hashkey'           => 'fbb4a673f0f8dc459635348f97f8803b8b3b310f'
        ),
		'url_submit'        => 'https://stbfep.sps-system.com/f01/FepMrcInfoReceive.do',
		'url_api'           => 'http://debug-2-pw-jp.fraise.jp/stub_sbps/sbps_api_response_stub.php?stub=1'
);
//[End][11318][11606] - [cu_lac] - [14/06/2016] - config sbps info

/**
 * Access restriction by IP Address
 * @application/core/PW_Authentication.php
 */
$config['permit_ip_address_admin'] = array(
    "219.111.2.137",    // Wakka Inc.
    "113.161.71.108",   // Lampart VNPT
    "118.69.191.57",    // Lampart FPT
);

$config['permit_ip_address_staff'] = array(
    "219.111.2.137",    // Wakka Inc.
    "113.161.71.108",   // Lampart VNPT
    "118.69.191.57",    // Lampart FPT
);

$config['permit_ip_address_cgi'] = array(
    "219.111.2.137",    // Wakka Inc.
    "113.161.71.108",   // Lampart VNPT
    "118.69.191.57",    // Lampart FPT
    "121.83.133.86",    // 業務サイト
    "221.244.176.210",  // IVR
    "180.42.51.145",    // IVR
    "61.215.213.47",    // Carrier payment SBPS server
); 
