<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 | Create default url of module admin|staff|mypage|registration
 */
/*
 |--------------------------------------------------------------------------
 | Config domain And sub_domain
 |--------------------------------------------------------------------------
 | Input domain running
 | Ex: I use domain dev.lampart.com.vn
 | CASE $config['domain'] = 'dev.lampart.com.vn';
 |     Use domain $config['domain'] = 'dev.lampart.com.vn';
 |    Use sub_domain $config['domain'] = 'dev.lampart.com.vn';
 | CASE $config['domain'] = 'www.dev.lampart.com.vn';
 |     Use domain $config['domain'] = 'www.dev.lampart.com.vn';
 |    Use sub_domain $config['domain'] = 'dev.lampart.com.vn'; remove www.
 |
 */
$config['domain']           = 'dev.lampart.com.vn';

/*
 |--------------------------------------------------------------------------
 | Error Logging Threshold
 |--------------------------------------------------------------------------
 |
 | If you have enabled error logging, you can set an error threshold to
 | determine what gets logged. Threshold options are:
 | You can enable error logging by setting a threshold over zero. The
 | threshold determines what gets logged. Threshold options are:
 |
 |    0 = Disables logging, Error logging TURNED OFF
 |    1 = Error Messages (including PHP errors)
 |    2 = Debug Messages
 |    3 = Informational Messages
 |    4 = All Messages
 |
 | For a live site you'll usually only enable Errors (1) to be logged otherwise
 | your log files will fill up very fast.
 |
 */
$config['log_threshold'] = 4;

/*
|--------------------------------------------------------------------------
| Default Language
|--------------------------------------------------------------------------
|
| This determines which set of language files should be used. Make sure
| there is an available translation if you intend to use something other
| than english.
|
*/
//$config['language']    = 'english';
$config['language']    = 'japanese';
