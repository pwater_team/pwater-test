# Content included #
* Setup PWATER development enviroment [required]
* Structure of a [PWATER CONFIG](https://bitbucket.org/pwater_team/pwater-test)
* Config domain [required]
* Config connect database [required]
* Ignore basic HTTP authentication [required]
* Remove redirect http to https htaccess [optional]
# Setup PWATER development enviroment [required]#

* Step 1: Downloads config pwater for dev

![2017-01-03_100931.png](https://bitbucket.org/repo/6kgXeE/images/796429159-2017-01-03_100931.png)

* Step 2: Copy all file in [PWATER CONFIG](https://bitbucket.org/pwater_team/pwater-test) overwrite to root directory project PWATER

# Structure of a [PWATER CONFIG](https://bitbucket.org/pwater_team/pwater-test)#
-----------------------------------------
    /application
       /config
          /dev_config
             config.php    ---> config domain, log_threshold, language
             database.php  ---> config connect database
             index.php     ---> config ENVIRONMENT (development, testing, production)
             pwater.php    ---> config link communication third party
       /libraries
          /payment
             modenv_properties.php ---
                                      |--> config payment method credit card
             Paygent_module.php    ---
    .htaccess
    codeigniter.php

# Config domain #
-----------------------------------------
   In file /application/config/dev_config/config.php change value of key `domain` in array config:

```
#!php
$config['domain'] = 'dev.lampart.com.vn';
```

# Config connect database #
-----------------------------------------
   In file /application/config/dev_config/database.php change config, default connect to db14 (172.16.100.14):

```
#!php
$db['default']['hostname'] = '172.16.100.14';
$db['default']['username'] = 'pwater';
$db['default']['password'] = 'pwater';
$db['default']['database'] = 'pwater';
```

# Ignore basic HTTP authentication #
-----------------------------------------
   The system requires login when accessing.

![authentication_requires.png](https://bitbucket.org/repo/6kgXeE/images/517535770-authentication_requires.png)

   In file /codeigniter.php comment out functions chk_admin(), chk_staff(), chk_cgi():

```
#!php
/*
 * ------------------------------------------------------
*  check authority
* ------------------------------------------------------
*/
require_once(APPPATH.'core/PW_Authentication.php');
switch (current_module()){
    case 'admin':
        //chk_admin();
        break;
    case 'staff':
        //chk_staff();
        break;
    case 'cgi':
        //chk_cgi();
        break;
    default:
        break;
}
```

# Remove redirect http to https htaccess #
In file /.htaccess, find a text `対象のディレクトリがhttpsじゃなければhttpsにする` and comment code, like the code below
```
#!php
#### 対象のディレクトリがhttpsじゃなければhttpsにする
#RewriteCond %{REQUEST_URI} ^/(_staff|_admin|mypage|registration|boothregist|campaign|landing|common|_cgi).* [NC]
#RewriteCond %{SERVER_PORT} 80
#RewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]
```